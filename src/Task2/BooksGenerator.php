<?php

declare(strict_types=1);

namespace App\Task2;

class BooksGenerator
{
    public int $minPagesNumber;
    public array $libraryBooks = array();
    public int $maxPrice;
    public array $storeBook;
    public array $reqBooks = array();


    public function __construct(int $minPagesNumber, array $libraryBooks, int $maxPrice, array $storeBook)
    {
        $this->minPagesNumber = abs($minPagesNumber);
        $this->libraryBooks = $libraryBooks;
        $this->maxPrice = abs($maxPrice);
        $this->storeBook = $storeBook;
    }


    public function generate(): \Generator
    {

        foreach ($this->libraryBooks as $book) {
            if ($book->getPagesNumber() >= $this->minPagesNumber) {
                $this->reqBooks[] = new Book($book->getTitle(), $book->getPrice(), $book->getPagesNumber());
            }
        }
        foreach ($this->storeBook as $book) {
            if ($book->getPrice() <= $this->maxPrice) {
                $this->reqBooks[] = new Book($book->getTitle(), $book->getPrice(), $book->getPagesNumber());
            }
        }
        foreach ($this->reqBooks as $book) {
            yield $book;
        }

    }
}