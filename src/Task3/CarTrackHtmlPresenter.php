<?php

declare(strict_types=1);

namespace App\Task3;

use App\Task1\Car;
use App\Task1\Track;

class CarTrackHtmlPresenter
{
    public function present(Track $track): string
    {
        $cars = $track->all();
        $carHtml = "<div class='backgrounder'>";

        foreach ($cars as $car) {
            $carHtml .= '<div class="car">';
            $carHtml .= '<p class="name"><strong>' . $car->getName() . '</strong></p>';
            $carHtml .= '<img src="' . $car->getImage() . '">';
            $carHtml .= '<p>Speed: ' . $car->getSpeed() . ' km/h' . '</p>';
            $carHtml .= '<p>Pit Stop Time: ' . $car->getPitStopTime() . ' seconds' . '</p>';
            $carHtml .= '<p>Fuel Consumption: ' . $car->getFuelConsumption() . ' litres' . '</p>';
            $carHtml .= '<p>Fuel Tank Volume: ' . $car->getFuelTankVolume() . ' litres per 100 km ' . '</p>';
            $carHtml .= '</div>';
        }
        $carHtml .= '</div>';

        return $carHtml;
    }
}