<?php

declare(strict_types=1);

namespace App\Task1;

class Car
{
    // First step - initialize field. Public
    public int $id;
    public string $image;
    public string $name;
    public int $speed;
    public int $pitStopTime;
    public float $fuelConsumption;
    public float $fuelTankVolume;

    // second step - fill in the fields via construct
    public function __construct(
        int $id,
        string $image,
        string $name,
        int $speed,
        int $pitStopTime,
        float $fuelConsumption,
        float $fuelTankVolume
    ) { // second step :)
        $this->id = $id;
        $this->image = $image;
        $this->name = $name;
        $this->speed = abs($speed);
        $this->pitStopTime = abs($pitStopTime);
        $this->fuelConsumption = abs($fuelConsumption);
        $this->fuelTankVolume = abs($fuelTankVolume);
    }

    // functions return value from fields
    public function getId(): int
    {
        return $this->id;
    }

    public function getImage(): string
    {
        return $this->image;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSpeed(): int
    {
        return $this->speed;
    }

    public function getPitStopTime(): int
    {
        return $this->pitStopTime;
    }

    public function getFuelConsumption(): float
    {
        return $this->fuelConsumption;
    }

    public function getFuelTankVolume(): float
    {
        return $this->fuelTankVolume;
    }
}