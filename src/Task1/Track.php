<?php

declare(strict_types=1);

namespace App\Task1;

class Track
{
    public float $lapLength;
    public int $lapsNumber;
    public array $trackList=array();
    public function __construct(float $lapLength, int $lapsNumber)
    {
        $this->lapLength = abs($lapLength);
        $this->lapsNumber = abs($lapsNumber);
    }

    public function getLapLength(): float
    {
        return $this->lapLength;
    }

    public function getLapsNumber(): int
    {
        return $this->lapsNumber;
    }

    public function add(Car $car): void
    {
        array_push($this->trackList,$car);
    }

    public function all(): array
    {
        return $this->trackList;
    }

    public function run(): Car
    {
        $allCarData = $this->all();
        $allCarTime = array();
        $allLength = $this->getLapLength() * $this->getLapsNumber();

        foreach ($allCarData as $carItem)
        {
            $pathTimeItem = round($allLength/($carItem->speed),2) * 60;
            $fullPathFuelCar = floor( ($carItem->fuelTankVolume * 100) / $carItem->fuelConsumption );
            $pitStopRelation = floor($allLength/$fullPathFuelCar);

            if ($pitStopRelation >= 1)
            {
                $pathTimeItem += $carItem->pitStopTime * $pitStopRelation;
            }

            $allCarTime[$carItem->id] = $pathTimeItem;
        }

        $min = "";
        $minKey = "";

        foreach ($allCarTime as $keyTime => $valTime )
        {
            if ($valTime < $min || $min == "")
            {
                $min = $valTime;
                $minKey = $keyTime;
            }
        }

        $finishValue = "";

        foreach ($allCarData as $key => $value)
        {
            if ($value->id == $minKey)
            {
                $finishValue = $allCarData[$key];
            }
        }
        return  $finishValue;
    }
}